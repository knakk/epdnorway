# Information

# EPD Norway

Open XML API for lookup of LCA-based data from Environmental Product Declarations (EPDs) by manufacturers.


## Focus point in data

	1) Indicators of life cycle
	2) Indicators of the impact assessment

## Example EPD XML lookup

	`https://epdnorway.lca-data.com/resource/datastocks/91413340-7bf0-4f88-a952-0f91cba685df/processes/c6990e77-9222-47a4-80a6-dd66edaef1ea?format=xml&lang=en`

# Python module packaging

	taken from: https://pypi.org/project/pypi-install/

	sudo apt install python-pip
	pip install pypi-install

## Package and upload

```
from pypi_install import pypi_install as PI
pypirc_username="my_pypi_username"
pypirc_password= "my_pypi_password"
directory_of_new_folder = "path_new_directory" # for e.g. "/home/USER/New_Folder"
version_number="0.0.1"
author_name_full="Knakk AS"
author_email="knakk@knakk.no"
short_description="Consume XML from EPD Norway API"
github_url="https://github.com/knakk/epdnorway"
python_version="2"
name_of_project="epdnorway"
directory_of_python_files=["PATH/my_python_file_1.py","PATH/my_python_file_2.py"] # Include all the python files here

# Upload
make_pypi_folders(pypirc_username,pypirc_password,directory_of_new_folder,name_of_project,directory_of_python_files,version_number,author_name_full,author_email,short_description,github_url,python_version,invoke_python_by_name='python',license_type="MIT License",operating_system="OS Independent")
```
